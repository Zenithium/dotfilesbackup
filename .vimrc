set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" --------------------------------
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'git://git.wincent.com/command-t.git'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'jiangmiao/auto-pairs'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'preservim/nerdtree'
" --------------------------------
call vundle#end()
filetype plugin indent on
" --------------------------------
let g:airline_powerline_fonts = 1
" --------------------------------
highlight LineNr ctermfg=gray ctermbg=238
highlight Normal ctermfg=15 ctermbg=235
highlight EndOfBuffer ctermfg=bg ctermbg=bg
highlight VertSplit ctermfg=238
set fillchars+=vert:\ 
" --------------------------------
autocmd VimEnter * NERDTree | wincmd p
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:airline#extensions#tabline#enabled = 1
set number
set relativenumber
set autoindent
set backspace=indent,eol,start
syntax on
set undofile
set backup
set swapfile
set undodir=$HOME/.vim/swap
set backupdir=$HOME/.vim/swap
set directory=$HOME/.vim/swap
" --------------------------------
command -nargs=1 Te tabedit <args>
command Tc tabclose
command Tp tabp
command Tn tabn
" --------------------------------
