# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle :compinstall filename '/home/zen/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# End of lines configured by zsh-newuser-install

PROMPT="%F{green}%n%f%F{8}@%f%F{green}%m%f%F{8}:%f %F{cyan}%~%f %F{green}$%f "
RPROMPT="%F{green}%t%f%F{8}@%f%F{green}%w%f"
pfetch
