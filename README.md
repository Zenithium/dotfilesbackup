# Zenithium's dotfiles

Migrated from [GitHub](https://www.github.com/ZenithiumDev/dotfilesbackup).
### What's included?
I included my:
  - vimrc ([Vundle](https://www.github.com/VundleVim/Vundle.vim) required)
  - slackware bashrc (paths so package management works with `sudo`)
  - [bash](https://www.gnu.org/software/bash/)rc (latest as of 18/11/2020 DD/MM/YYYY)
  - [awesomewm](https://awesomewm.org/) config
  - [dwm](https://dwm.suckless.org) config
  - [dmenu](https://tools.suckless.org/dmenu) config
  - [st](https://st.suckless.org) config
  - [zsh](https://www.zsh.org/)rc (latest as of 01/11/2020 DD/MM/YYYY)
